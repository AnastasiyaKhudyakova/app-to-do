export const TODOS_DB_SCHEMA = `CREATE TABLE IF NOT EXISTS todos (
  user_id INT NOT NULL,
  task_id INT AUTO_INCREMENT, 
  task VARCHAR(255) NOT NULL,
  is_completed BOOLEAN NOT NULL DEFAULT FALSE,
  priority BOOLEAN NOT NULL DEFAULT TRUE,
  due_date DATETIME NOT NULL,
  created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (task_id),
  FOREIGN KEY (user_id)
  REFERENCES users (user_id)
  ON UPDATE RESTRICT ON DELETE CASCADE
   );`;

export const SELECT_TODOS = 'SELECT * FROM todos WHERE user_id = ?';
export const SELECT_TODO = 'SELECT * FROM todos WHERE user_id = ? AND task_id = ?';
export const UPDATE_TODO = 'UPDATE todos SET ? WHERE user_id = ? AND task_id = ?';
export const ADD_TODO = 'INSERT INTO todos SET ?';
export const DELETE_TODO = 'DELETE FROM todos WHERE user_id = ? AND task_id = ?';
