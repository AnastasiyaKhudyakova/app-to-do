export const USER_DB_SCHEMA = `CREATE TABLE 'users' (
  user_id INT NOT NULL AUTO_INCREMENT,
  name varchar(50) NOT NULL,
  password varchar(255) NOT NULL,
  email varchar(100) NOT NULL UNIQUE,
  created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (user_id)
);`;

export const SELECT_USER = 'SELECT * FROM users WHERE email = ?';
export const ADD_USER = 'INSERT INTO users SET ?';
