import bcrypt from 'bcrypt';

const SALT_ROUNDS = 10;

const bcryptUserPassword = async (password: string): Promise<string> => {
  const salt = await bcrypt.genSaltSync(SALT_ROUNDS);

  const hash = await bcrypt.hashSync(password, salt);

  return hash;
};

const checkUserPassword = async (password: string, hash: string) => {
  const result = await bcrypt.compare(password, hash);

  return result;
};

export {
  bcryptUserPassword,
  checkUserPassword,
};
