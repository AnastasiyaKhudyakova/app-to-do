import express from 'express';
import jwt from 'jsonwebtoken';
import { connection } from '../index';
import { IAuthRequest, IJwtPayload } from '../types/users';

const authenticateJwt = (
  req: express.Request,
  res: express.Response,
  next: express.NextFunction,
) => {
  const authHeader = req.headers.authorization;

  if (authHeader) {
    const token = authHeader.split(' ')[1];

    jwt.verify(token, `${process.env.ACCESS_TOKEN_SECRET}`, (verifErr, verifResult) => {
      if (verifErr) {
        return res.status(403).json({
          message: 'Authentication required',
        });
      }

      const { id, email } = jwt.decode(token) as IJwtPayload;

      connection.query(
        'SELECT * FROM users WHERE user_id = ? AND email = ?',
        [id, email],
        (usersError, usersResult) => {
          if (usersError || !usersResult[0]) {
            return res.status(403).json({
              message: 'Authentication required',
            });
          }

          (req as IAuthRequest).user = usersResult[0];

          next();
        },
      );
    });
  } else {
    res.status(401).json({
      message: 'Unauthorized',
    });
  }
};

export { authenticateJwt };
