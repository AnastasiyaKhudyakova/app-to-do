export interface IExpressError extends Error {
  status: number;
}

export type KeysEnum<T> = { [P in keyof Required<T>]: true };
