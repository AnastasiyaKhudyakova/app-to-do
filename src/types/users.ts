import { Request } from 'express';

export interface IUserFormData {
  [key: string]: string | number |undefined;
  name: string;
  email: string;
  password: string;
}

export interface IUser extends IUserFormData{
  readonly id?: number;
  readonly createdAt?: string;
}

export interface IAuthRequest extends Request {
  user: IUser;
}

export interface IJwtPayload {
  email: string;
  name: string;
  id: number;
  expiresIn: number | string;
  iat: number;
}
