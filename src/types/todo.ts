export interface IToDoFormData {
  task: string;
  priority: boolean;
  is_completed: boolean;
  due_date: string;// 'YYYY-MM-DD'
  task_id: number;
}

export interface IToDo extends IToDoFormData {
  readonly user_id: number;
  readonly task_id: number;
  readonly created_at: string;
}
