/* eslint-disable import/no-unresolved */
/* eslint-disable import/extensions */
import express, { NextFunction } from 'express';
import bodyParser from 'body-parser';
import morgan from 'morgan';
import cors from 'cors';
import mysql from 'mysql';
import { IExpressError } from './types/main';
import tasksRouter from './routes/todos';
import userRouter from './routes/users';
import { TODOS_DB_SCHEMA } from './db/todos';
import { USER_DB_SCHEMA } from './db/users';

require('dotenv').config();

export const connection = mysql.createConnection({
  host: 'localhost',
  user: 'to-do-app',
  password: '',
  database: 'to_do_app_db',
});

connection.connect((err) => {
  if (!err) console.log(err);
});

connection.query(TODOS_DB_SCHEMA, (createToDoTableErr) => {
  if (createToDoTableErr!) {
    connection.query(USER_DB_SCHEMA, (createUserTableErr) => {
      if (createUserTableErr) {
        throw Error('Error');
      }
    });
  }
});

const port = process.env.PORT;

const app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(morgan('dev'));
app.use(cors());

// manage cors
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', '*');

  if (req.method === 'OPTIONS') {
    res.header('Access-Control-Allow-Methods', 'PUT POST PATCH DELETE GET');

    return res.status(200).json({});
  }
  next();
});

app.use('/todos', tasksRouter);
app.use('/auth', userRouter);

// Error handlers
app.use('/', (
  req: express.Request,
  res: express.Response,
  next: NextFunction,
) => {
  const error = new Error('Not found');

  const expressError: IExpressError = {
    ...error,
    message: 'Not Found',
    status: 404,
  };

  next(expressError);
});

app.use('/', (
  error: IExpressError,
  req: express.Request,
  res: express.Response,
  next: NextFunction,
) => {
  res.status(error.status || 500);
  res.json({
    error: {
      message: error.message,
      status: error.status,
    },
  });
});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});
