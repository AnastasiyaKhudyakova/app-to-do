import express from 'express';
import { connection } from '../index';
import { IAuthRequest } from '../types/users';
import { authenticateJwt } from '../utils/auth';
import { validateData } from '../validators/main';
import { isValidToDoFormData, todoDataValidationRules } from '../validators/todos';
import {
  SELECT_TODOS, SELECT_TODO, UPDATE_TODO, ADD_TODO, DELETE_TODO,
} from '../db/todos';

const router: express.Router = express.Router();

router.get('/', authenticateJwt, (req: express.Request, res: express.Response) => {
  const { user } = req as IAuthRequest;

  connection.query(
    SELECT_TODOS,
    user.user_id,
    (error, result, fields) => {
      res.status(200).json({
        tasks: result,
        status: 200,
      });
    },
  );
});

router.get('/:id', authenticateJwt, (req: express.Request, res: express.Response) => {
  const { id } = req.params;

  const { user } = req as IAuthRequest;

  connection.query(
    SELECT_TODO,
    [user.user_id, id],
    (error, result) => {
      if (error) {
        return res.status(500).json({
          data: result,
          error,
        });
      }

      if (result?.length) {
        return res.status(200).json({
          data: result,
        });
      }

      return res.status(404).json({
        message: 'Task not found',
      });
    },
  );
});

router.post('/', authenticateJwt, (req: express.Request, res: express.Response) => {
  if (!isValidToDoFormData(req.body)) {
    return res.status(400).json({
      message: 'Invalid form data',
    });
  }

  const valiadtionErrors = validateData(req.body, todoDataValidationRules);

  if (valiadtionErrors.length) {
    return res.status(400).json({
      message: 'Invalid field values',
      data: valiadtionErrors,
    });
  }

  const { user } = req as IAuthRequest;

  const {
    task, priority, is_completed, due_date, task_id,
  } = req.body;

  if (task_id >= 1) {
    connection.query(
      SELECT_TODO,
      [user.user_id, task_id],
      (error, result, fields) => {
        if (error || !result.length) {
          return res.status(404).json({
            message: 'Task not found',
          });
        }

        if (result.length) {
          const updatedTaskValues = req.body;

          const previosValue = result[0];

          const newValue = { ...previosValue, ...updatedTaskValues };

          connection.query(
            UPDATE_TODO,
            [newValue, user.user_id, task_id],
            (updError, updResult) => {
              if (updResult) {
                return res.status(201).json({
                  message: 'Update success',
                  data: newValue,
                });
              }

              if (updError) {
                return res.status(400).json({
                  message: 'Update failed',
                  error: updError,
                });
              }
            },
          );
        }
      },
    );
  } else {
    const taskData = {
      user_id: user.user_id,
      task,
      priority,
      is_completed,
      due_date,
    };

    connection.query(
      ADD_TODO,
      taskData,
      (error, result, fields) => {
        if (result) {
          return res.status(201).json({
            message: 'Task added',
            data: {
              ...taskData,
              task_id: result.insertId,
              created_at: result.created_at,
            },
          });
        }

        if (error) {
          return res.status(400).json({
            message: 'Error',
            error,
          });
        }
      },
    );
  }
});

router.delete('/:id', authenticateJwt, (req: express.Request, res: express.Response) => {
  const { id } = req.params;

  const { user } = req as IAuthRequest;

  connection.query(
    DELETE_TODO,
    [user.user_id, id],
    (error, result, fields) => {
      if (result) {
        return res.status(201).json({
          message: 'Task deleted',
          id,
        });
      }

      if (error) {
        return res.status(404).json({
          message: 'Task not found',
          error,
        });
      }
    },
  );
});

const tasksRouter = router;

export default tasksRouter;
