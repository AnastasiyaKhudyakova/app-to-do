import express from 'express';
import jwt from 'jsonwebtoken';
import { connection } from '../index';
import { IUserFormData } from '../types/users';
import { bcryptUserPassword, checkUserPassword } from '../utils/pswdBcrypt';
import { validateData } from '../validators/main';
import { isValidUserFormData, userDataValidationRules } from '../validators/user';
import { SELECT_USER } from '../db/users';

const router: express.Router = express.Router();

router.post('/signIn', (req: express.Request, res: express.Response) => {
  const { email, password } = req.body;

  if (!isValidUserFormData({ email, password })) {
    return res.status(400).json({
      message: 'Invalid form data',
    });
  }

  connection.query(SELECT_USER, email, (err, result) => {
    if (result?.length === 1) {
      checkUserPassword(password, result[0].password)
        .then((isValidPsw: boolean) => {
          if (!isValidPsw) {
            throw Error('Ivalid password');
          }

          const tokenPayload = {
            email,
            name: result[0].name,
            id: result[0].user_id,
            expiresIn: '2h',
          };

          const accessToken = jwt.sign(tokenPayload, process.env.ACCESS_TOKEN_SECRET || '');

          return res.status(201).json({
            message: 'Login Success',
            userData: {
              email,
              name: result[0].name,
              id: result[0].user_id,
              idToken: accessToken,
            },
          });
        })
        .catch((error: Error) => res.status(400).json({
          message: 'Invalid Password',
        }));

      return;
    }

    if (!result || !result?.length || err) {
      return res.status(404).json({
        message: 'User not found',
      });
    }
  });
});

router.post('/signUp', (req: express.Request, res: express.Response) => {
  const { name, email, password } = req.body;

  if (!isValidUserFormData({ name, email, password })) {
    return res.status(400).json({
      message: 'Invalid field data',
    });
  }

  const userCredentials: Partial<IUserFormData> = { name, email, password };

  const valiadtionErrors = validateData(userCredentials, userDataValidationRules);

  if (valiadtionErrors.length) {
    return res.status(400).json({
      message: 'Signup failed',
      data: valiadtionErrors,
    });
  }

  bcryptUserPassword(password)
    .then((hash: string) => {
      connection.query(SELECT_USER, email, (err, result) => {
        if (!result) {
          const userData = {
            name: name.trim(),
            email: email.trim(),
            password: hash,
          };

          connection.query('ADD_USER', userData, (signUpError, signUpSuccess) => {
            if (signUpSuccess) {
              const tokenPayload = {
                email: userData.email,
                name: userData.name,
                id: signUpSuccess.insertId,
                expiresIn: '2h',
              };

              const accessToken = jwt.sign(tokenPayload, process.env.ACCESS_TOKEN_SECRET || '');

              return res.status(201).json({
                message: 'Signup Success',
                userData: {
                  name,
                  email,
                  idToken: accessToken,
                  id: signUpSuccess.insertId,
                },
              });
            }

            return res.status(400).json({
              message: 'Email is already in use',
              error: new Error('Email is already in use'),
            });
          });
        } else {
          return res.status(400).json({
            message: 'Signup failed',
          });
        }
      });
    })
    .catch((error) => res.status(500).json({
      message: 'Server error. Try again later',
    }));
});

const userRouter = router;

export default userRouter;
