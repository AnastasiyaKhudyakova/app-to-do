import { KeysEnum } from '../types/main';
import { IUserFormData } from '../types/users';
import { IValidationRulesMap } from './main';

const userDataValidationRules: IValidationRulesMap = {
  name: {
    required: true,
    minLen: 3,
    maxLen: 20,
    onlyNumbersAndLetters: true,
    isValidType: 'string',
  },
  email: {
    required: true,
    isEmail: true,
    isValidType: 'string',
  },
  password: {
    required: true,
    isStrongPassword: true,
    maxLen: 20,
    isValidType: 'string',
  },
};

const userFormKeys: KeysEnum<IUserFormData> = {
  name: true,
  email: true,
  password: true,
};

const isValidUserFormData = (data: {[key: string]: any}) => {
  const isValid = Object.keys(data)
    .every((prop: string) => data[prop] && prop in userFormKeys);

  return isValid;
};

export { isValidUserFormData, userDataValidationRules };
