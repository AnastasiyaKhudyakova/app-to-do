/* eslint-disable max-len */
/* eslint-disable no-control-regex */

const EMAIL_REGEX = /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/;

export type ValidatorsMap = {
  [key: string]: (...args: any) => boolean;
}

export type ValidationErrMessage = {
  [key: string]: (...args: any) => string;
}

export type IValidationRulesMap = {
  [key: string]: IValidationRules;
}

export interface IValidationRules {
  [key: string]: number | boolean | string | undefined;
  required?: boolean;
  minLen?: number;
  maxLen?: number;
  onlyNumbersAndLetters?: boolean;
  isStrongPassword?: boolean;
  isEmail?: boolean;
  isValidType?: string;
}

export const VALIDATORS: ValidatorsMap = {
  required: <T>(value: T) => (typeof value === 'boolean' ? true : !!value),
  maxLen: (value: string, max: number) => value.length <= max,
  minLen: (value: string, min: number) => value.length >= min,
  onlyNumbersAndLetters: (value: string) => /^(?!\s+)[A-Za-z0-9]+$/.test(value),
  isStrongPassword: (value: string) => /^(?!\s+)(?=.*[A-Z]+)[A-Za-z0-9]{8,}$/.test(value),
  isEmail: (value: string) => EMAIL_REGEX.test(value),
  // eslint-disable-next-line valid-typeof
  isValidType: (value: any, type: string) => typeof value === type,
};

export const validationErrMessageMap: ValidationErrMessage = {
  required: (field: string) => `${field} is required`,
  maxLen: (field: string, max: number) => `${field} length musn't exceed ${max} characters`,
  minLen: (field: string, min: number) => `${field} length musn't be less than ${min} characters`,
  onlyNumbersAndLetters: (field: string) => `${field} must contain only numbers and letters`,
  isStrongPassword: (field: string) => `${field} must contain at least 8 numbers or letters and one capital letter`,
  isEmail: (field: string) => `${field} is invalid`,
  isValidType: (field: string, type: string) => `${field} must be of type ${type}`,
};

const validateData = <T>(data: {[key: string]: T}, validationRules: IValidationRulesMap): string[] => {
  const errors: string[] = [];

  Object.keys(data).forEach((field: string) => {
    const rulesEntry: IValidationRules = validationRules[field];

    if (rulesEntry) {
      Object.keys(rulesEntry).forEach((rule) => {
        let isValid: boolean = true;

        if (typeof rulesEntry[rule] !== 'boolean') {
          isValid = VALIDATORS[rule](data[field], rulesEntry[rule]);
        } else {
          isValid = VALIDATORS[rule](data[field]);
        }

        if (!isValid) {
          const fieldValue = rulesEntry[rule];

          let errorMessage: string = '';

          if (typeof fieldValue !== 'boolean') {
            errorMessage = validationErrMessageMap[rule](field, fieldValue);
          } else {
            errorMessage = validationErrMessageMap[rule](field);
          }

          if (errorMessage) {
            errors.push(errorMessage);
          }
        }
      });
    }
  });

  return errors;
};

export { validateData };
