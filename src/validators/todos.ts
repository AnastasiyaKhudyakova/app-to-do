import { IValidationRulesMap } from './main';
import { IToDoFormData } from '../types/todo';
import { KeysEnum } from '../types/main';

const todoDataValidationRules: IValidationRulesMap = {
  user_id: {
    required: true,
  },
  task_id: {
    required: true,
    isValidType: 'number',
  },
  task: {
    required: true,
    minLen: 20,
    maxLen: 200,
    isValidType: 'string',
  },
  priority: {
    required: true,
    isValidType: 'boolean',
  },
  is_completed: {
    required: true,
    isValidType: 'boolean',
  },
  due_date: {
    isValidType: 'string',
  },
};

const toDoFormKeys: KeysEnum<IToDoFormData> = {
  task: true,
  priority: true,
  is_completed: true,
  due_date: true,
  task_id: true,
};

const isValidToDoFormData = (data: {[key: string]: any}) => {
  const isValid = Object.keys(data)
    .every((prop: string) => prop in toDoFormKeys);

  return isValid;
};

export { isValidToDoFormData, todoDataValidationRules };
